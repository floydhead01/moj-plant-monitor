#include "Adafruit_BLE.h"
#include "Adafruit_BluefruitLE_SPI.h"
#include "Adafruit_BluefruitLE_UART.h"
#include "Adafruit_seesaw.h"

#define RTS  10
#define RXI  11
#define TXO  12
#define CTS  13
#define MODE -1

#define BUFSIZE 128
#define VERBOSE_MODE false
#define BLUEFRUIT_SPI_CS               8
#define BLUEFRUIT_SPI_IRQ              7
#define BLUEFRUIT_SPI_RST              4    
Adafruit_BluefruitLE_SPI ble(BLUEFRUIT_SPI_CS, BLUEFRUIT_SPI_IRQ, BLUEFRUIT_SPI_RST);

//Soil Sensor
Adafruit_seesaw ss;

int battery = 0;
int count = 0;
String BLE_NAME = "PLANTPI";

void setup() {
  Serial.begin(115200);
  Serial.println(F("Starting Radio:"));

  delay(5000);

  if (! ble.begin(VERBOSE_MODE)) {
    Serial.println( F("FAILED BLE SETUP!") );
    while(1);
  }
  Serial.println("OK");



  Serial.print(F("Factory reset: "));
  if(! ble.factoryReset()) {
    Serial.println(F("FAILED."));
    while(1);
  }
  Serial.println("OK");


  Serial.print(F("Set device name: "));
  if(! ble.sendCommandCheckOK("AT+GAPDEVNAME=PMD_MoMoney")) {
    Serial.println(F("FAILED."));
    while(1);
  }

  Serial.println("OK");


  Serial.print(F("Setting up Solil Sensor: "));
  if (!ss.begin(0x36)) {
    Serial.println(F("FAILED."));
    while(1);
  }

  Serial.println("OK");

  Serial.println(F("Device Name Set To: PMD_Cheesy"));
  Serial.println(ss.getVersion(), HEX);

  

  ble.echo(false);

  batteryLevel();

}

void loop() {

  while (! ble.isConnected()){
    Serial.println("Not Connected");
    delay(1000);
  }

  Serial.println("Connected!");

  
  float tempC = ss.getTemp();
  uint16_t capread = ss.touchRead(0);
  Serial.print(tempC);
  
  ble.print("AT+BLEUARTTX=");
  ble.print(tempC, 1);
  ble.print(",");
  ble.print(capread, 1);
  ble.print(",");
  ble.print(battery, DEC);
  ble.println("|");
  ble.readline(200);

  if(count == 100) {
    batteryLevel();
    count = 0;
  } else {
    delay(1000);
    count++;
  }
}

void batteryLevel() {

  #define VBATPIN A7
   
  float measuredvbat = analogRead(VBATPIN);
  measuredvbat *= 2;    // we divided by 2, so multiply back
  measuredvbat *= 3.3;  // Multiply by 3.3V, our reference voltage
  measuredvbat /= 1024; // convert to voltage
  Serial.print("VBat: " ); Serial.println(measuredvbat);

  ble.println("AT+HWVBAT");
  ble.readline(1000);

  if(strcmp(ble.buffer, "OK") == 0) {
    battery = 0;
  } else {
    battery = atoi(ble.buffer);
    ble.waitForOK();
  }
  //battery =  measuredvbat;

}
