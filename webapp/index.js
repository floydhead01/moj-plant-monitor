
const express = require('express')
var bodyParser = require('body-parser')
var cors = require('cors')
const GetDB = require('./db.js')


const app = express()
const port = 3333


app.use(bodyParser.json());
app.use(cors())

app.post('/api/data', function(request, response){
    var data = request.body;
    console.log(data);
    GetDB((db) => {
        var stmt = db.prepare("INSERT INTO sensors VALUES (?,?,?,?,?)");
        stmt.run(data.device, new Date().toISOString(), data.temp, data.moistness, data.battery);
        stmt.finalize();
        response.send({result: "OK"});


    })
    //console.log(request.body);      // your JSON
    //response.send(request.body);    // echo the result back
});

app.get('/api/data/devices', function(request, response){
    GetDB((db) => {
        db.all("SELECT device, count(*) as 'events' FROM sensors GROUP BY device", (err, rows)=>{
            console.log(rows);
            response.send(rows);

        })
    })
    
})

app.get('/api/data', function(request, response){
    var device = request.query.device
    console.log(device);
    GetDB((db) => {
        db.all("SELECT * FROM sensors WHERE device = '" + device + "'", (err, rows)=>{
            console.log(rows);
            rows.sort(function(a,b){
                // Turn your strings into dates, and then subtract them
                // to get a value that is either negative, positive, or zero.
                return new Date(a.timestamp) - new Date(b.timestamp);
              });
            let data = rows;
            if(rows.length ){
                data = rows.slice(Math.max(rows.length - 150, 1))
            }
            response.send(data);

        })
    })
    
})

app.use('/', express.static('public'))

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

