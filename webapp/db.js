var sqlite3 = require('sqlite3').verbose();
const fs = require('fs')

const db_path = './db/plant.db';
let setup = false;
let db = null;

function _makedb(callback){
    if(setup && db != null) {
        callback(db); return;
    }

    try {
        var db_exists = fs.existsSync(db_path);
        db = new sqlite3.Database(db_path, (err) => {
            if (err) {
              console.error(err.message);
              return;
            }
    
            console.log('Connected to database');
            if(!db_exists) {
                db.run("CREATE TABLE sensors (device TEXT, timestamp TEXT, temp REAL, moistness REAL, battery REAL)", (err) => {
                    setup = true;
                    callback(db);
                });
            } else {
                setup = true;
                callback(db);
            }
            
        });
        
        
    } catch (err) {
        console.error(err);
    }
}



function getDatabase(callback){
    return _makedb(callback)
}

module.exports = getDatabase;
