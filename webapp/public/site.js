var REQUEST = {
    post: function(url, data, callback){
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (this.readyState != 4) return;
            if (this.status == 200) {
                var data = JSON.parse(this.responseText);
                callback(data);
            }
        }
        xhr.open("POST", url, true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.send(JSON.stringify(data));

    },
    get: function(url, callback){
        console.log("test")
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (this.readyState != 4) return;
            if (this.status == 200) {
                var data = JSON.parse(this.responseText);
                callback(data);
            }
        }
        xhr.open('GET', url, true);
        xhr.send();
    }
}

var app = {
    render: function(name){
        console.log(name);
        REQUEST.get("http://127.0.0.1:3333/api/data?device=" + name, (data) => {

            
            var div = document.createElement("DIV");
            div.classList.add("plant");

            if(data[data.length -1].moistness < 500) {
                div.classList.add("caution");
            }
            if(data[data.length -1].moistness < 700 && data[data.length -1].moistness >= 500) {
                div.classList.add("warning");
            }


            var y = document.createElement("H1");
            y.innerText = name;

            var x = document.createElement("CANVAS"); 
            x.style.height = "150px";
            x.style.width = "600px";
            x.id = "chart_" + name;

            div.appendChild(y);
            div.appendChild(x);
            

            document.getElementById("app").append(div);
            console.log(name);
            var labels = [];
            var temp = [];
            var moistness = [];

            for(var i=0; i<data.length; i++){
                labels.push(data[i].timestamp);
                temp.push(data[i].temp);
                moistness.push(data[i].moistness)
            }
            
            var ctx = document.getElementById('chart_' + name);

            var chart_data = {
                labels: labels,
                datasets: [{
                    label: 'Temp',
                    data: temp,
                    yAxisID: 'A',
                    fill: false,
                    borderColor: 'rgb(75, 192, 192)',
                    tension: .5
                },
                {
                    label: 'Moisture',
                    data: moistness,
                    yAxisID: 'B',
                    fill: false,
                    borderColor: 'rgb(192, 70, 192)',
                    tension: .5
                }]
            };
            console.log(chart_data);
            const config = {
                type: 'line',
                data: chart_data,
                options: {
                    xAxes: [{
                        type: 'time',
                        ticks: {
                            autoSkip: true,
                            maxTicksLimit: 20
                        }
                    }]
                }
              };
              var myChart = new Chart(ctx, config);
        })

    },
    init: function(){
        REQUEST.get("http://52.87.235.68:3333/api/data/devices", function(data){
            for(var i=0; i<data.length; i++){
                app.render(data[i].device)
            }
            
        });
    }
}

document.addEventListener("DOMContentLoaded", function(event) { 
    app.init();
  });


