# MOJ Plant Monitor

A plant montioring solution, to track temp and soil mosture. The project has three main components. A sensor, that broadcasts readings across low energy bluetooth. A monitor that sits near the plants, grabs the various readings, then broadcasts them to a server and finally the server applicaiton which stores data and provides a simple dashboard to view results 

![Diagram](moj_plants.png)

## Parts List

For this project to work, you need the following parts. 

*Please note the arduino boards, and pi could be replaced, but code has not been tested on alternative configurations or devices.*

1. Feather M0 - https://www.adafruit.com/product/2995?hidden=yes&main_page=product_info&part_id=2995
2. i2c Soil Sensor https://www.adafruit.com/product/4026
3. Pi 3 A+ - https://www.raspberrypi.org/products/raspberry-pi-3-model-a-plus/
4. Appropriate connectors to connect I2C soil sensor to the Feathre m0
5. [Optional] LiPo battery - 3.7v to enable wireless monitoring 

## Sensor
The sensor itself is a simple arduino running a job which polls the sensor when something is reading from it. It cycles every 1 second looking for a connection. This can cause some weridness with connecting, if a device is trying to negotate, while it drops to sleep, but a simple retry can avoid this. Future itterations of the code will look to find a more graceful solve. 

### Compiling
The core code for the sensor is in ardunio tab. Make sure all dependends are install for your board and for the soil sensor, then build. 

Each device should a unique device name, as this is what gets sent to the middleware. To configure the device name, change the value on line 49. 

## Monitor. 
The monitor app is a simple python applicaiton that reads any BLE devices that start with PMD in the device name, grabs the wrtitten value then sends it to the middeleware 

### Compiling. 
To build the middleware setup your raspberry pie with the latest debian build. Note the app must run as root user. 

First update the python file to point to your-endpoint. 

```bash
pip3 install -r requirements.txt
sudo python3 app.py
```

### Install as service
A simple service is provided via plantmon.service. This can be installed on any system using systemd to install. 

1. plantmon.service to point to your python file on your device. 
2. run: sudo cp plantmon.service /etc/systemd/system/myscript.service
3. run: sudo systemctl daemon-reload

Your service should now be installed and can be started, stopped and monitored via systemctl. 

## WebApp
The webapplicaiton is a simple express app that has a few endpoints for uploading data from the monitor, and then getting history. 

*Note: The express app uses SQL Lite to store and presist data, it will automaticly build and deploy the database on any run where it cannot find the db file*

### Installing 
```bash
npm install
npm start
```