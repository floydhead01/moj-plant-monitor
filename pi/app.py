#!/usr/bin/python

from bluepy.btle import *
import time
import requests


class ScanDelegate(DefaultDelegate):
    def __init__(self):
        DefaultDelegate.__init__(self)

    def handleDiscovery(self, dev, isNewDev, isNewData):
        if isNewDev:
            print ("    Discovered device", dev.addr)
        elif isNewData:
            print ("    Received new data from", dev.addr)


def Scan():
    print("Starting Device Discovery:")
    scanner = Scanner().withDelegate(ScanDelegate())
    devices = scanner.scan(10.0)
    my_devices = []

    for dev in devices:
        #print ("Device %s (%s), RSSI=%d dB" % (dev.addr, dev.addrType, dev.rssi))
        for (adtype, desc, value) in dev.getScanData():
            #print ("  %s = %s" % (desc, value))
            if "PMD" in value:
                print("    Device Found %s"  % (dev.addr))

                my_devices.append({"Address": dev.addr, "AddressType": dev.addrType, "Name": value })
    
    print("End Discovery")
    print("Devices found: " + str(len(my_devices)))
    sys.stdout.flush()
    
    return my_devices

def getData(device):
    per = Peripheral(device["Address"], device["AddressType"])
    notify = per.getCharacteristics(uuid='6e400003-b5a3-f393-e0a9-e50e24dcca9e')[0]
    data = notify.read().decode("utf-8")

    print(device["Name"] + ":" + data)

    per.disconnect()
    split = data.replace("|","").split(",")

    return split


def main():
    my_devices = Scan()
    while True:
        timeout = 1800
        for device in my_devices:
            read = False
            while read is False:
                try:
                    time.sleep(5)
                    data = getData(device)
                    r = requests.post('http://xxx.xxx.xxx.xxx:xxxx/api/data/', json={"device": device["Name"], "temp": float(data[0]), "moistness": float(data[1]), "battery": float(data[2])})
                    read = True
                except:
                    print("Unexpected error:", sys.exc_info()[0])

        sys.stdout.flush()    
        time.sleep(timeout)

if __name__ == "__main__":
    main()


